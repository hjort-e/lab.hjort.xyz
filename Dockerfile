# build stage
FROM node:10.13.0-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# production stage
FROM nginx:1.13.12-alpine as production-stage
RUN rm -r /usr/share/nginx/html/*
COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY /nginx/default.conf /etc/nginx/conf.d/

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]